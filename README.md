# monopse 

Another simple tool for manage terminal applications.

> Monopse is a command line tool for start, stop and check status for applications specified by user
> as a parameter, find out how to use and install Monopse by reading this document.


## Developed by
Andres Aquino <inbox(at)andresaquino.info>

## Source Code
 * [BitBucket]    https://bitbucket.org/andresaquino/monopse
 * [GitHub]       http://andresaquino.github.com/monopse
 * [Bugtracker]   http://github.com/andresaquino/monopse/issues

## Documentation

Please, use the next flags to indicate the issue to implement

 Type       | Description
----------- | ---------------
DEFECT      | Report a Software Defect
ENHANCEMENT | Request for a Enhancement
TASK        | Work item that change code or documentation
REVIEW      | Request for a source code review 
OTHER       | Some other kind of issue 

## Tested with

 * Oracle WebLogic 11
 * Oracle WebLogic 10
 * BEA WebLogic 9.2
 * BEA WebLogic 8.1
 * BEA WebLogic 6
 * Targys (install screen)
 * local applications running on Linux and HP-UX

